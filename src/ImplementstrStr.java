/**
 * Implement strStr().
 *
 * Return the index of the first occurrence of needle in haystack, or -1 if needle is not part of haystack.
 *
 * Example 1:
 *
 * Input: haystack = "hello", needle = "ll"
 * Output: 2
 * Example 2:
 *
 * Input: haystack = "aaaaa", needle = "bba"
 * Output: -1
 */

public class ImplementstrStr {

    //use built in method indexOf

    public static int strStr(String haystack, String needle) {

        return haystack.indexOf(needle);

    }

    public static int strStr2(String haystack, String needle) {

        if(haystack == null || needle == null) return -1;

        int length1 = haystack.length();

        int length2 = needle.length();

        //length1-length2+1 represents when first char in needle is not existing in the this index we do not need to go through rest of the index

        for(int i = 0; i < length1-length2+1; i++){

            int j; //collect counts

            for (j=0; j<length2; j++){

                if(haystack.charAt(i+j)!=needle.charAt(j)){

                    break; // terminate for loop,move to next i, no need to compare rest of the j

                }

                //when for loop did not go through if statement above means haystack contains needle

            }

            if(j == length2) return i;

        }

        return -1;

    }



    public static void main(String args[]){

        String haystack1 = "hello";

        String needle1= "ll";

        String haystack2 = "aaaaa";

        String needle2= "bba";

        System.out.println(Integer.toString(strStr(haystack1,needle1))); //return 2

        System.out.println(Integer.toString(strStr(haystack2,needle2))); //return -1

        System.out.println(Integer.toString(strStr2(haystack1,needle1))); //return 2

        System.out.println(Integer.toString(strStr2(haystack2,needle2))); //return -1


    }
}
