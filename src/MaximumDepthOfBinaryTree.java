/**
 * Given a binary tree, find its maximum depth.
 *
 * The maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.
 *
 * Note: A leaf is a node with no children.
 *
 * Example:
 *
 * Given binary tree [3,9,20,null,null,15,7],
 *
 *     3
 *    / \
 *   9  20
 *     /  \
 *    15   7
 * return its depth = 3.
 */

public class MaximumDepthOfBinaryTree {

    public static int maxDepth(TreeNode root){

        if(root==null) return 0;

        if(root.left==null && root.right==null) return 1;

        return 1 + Math.max(maxDepth(root.left), maxDepth(root.right));

    }


    public static TreeNode sampleTreeNode() {

        TreeNode root = new TreeNode(3);
        TreeNode n2 = new TreeNode(9);
        TreeNode n3 = new TreeNode(20);
        TreeNode n6 = new TreeNode(15);
        TreeNode n7 = new TreeNode(7);

        root.left = n2;
        root.right = n3;
        n3.left = n6;
        n3.right = n7;
        return root;

    }

    public static void main(String args[]){

        TreeNode sample = sampleTreeNode();

        System.out.println(Integer.toString(maxDepth(sample)));

    }

}
