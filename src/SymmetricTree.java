import com.sun.source.tree.Tree;

/**
 * Given a binary tree, check whether it is a mirror of itself (ie, symmetric around its center).
 *
 * For example, this binary tree [1,2,2,3,4,4,3] is symmetric:
 *
 *     1
 *    / \
 *   2   2
 *  / \ / \
 * 3  4 4  3
 *
 *
 * But the following [1,2,2,null,3,null,3] is not:
 *
 *     1
 *    / \
 *   2   2
 *    \   \
 *    3    3
 *
 */

public class SymmetricTree {

    public static boolean isSymmetric(TreeNode root){

        if(root==null) return true;

        return helper(root.left, root.right);

    }

    public static boolean helper(TreeNode left, TreeNode right){

        if(left == null && right == null) return true;

        if(left == null || right == null) return false;

        return (left.val == right.val && helper(left.left, right.right) && helper(left.right, right.left));

    }

    //create sample input TreeNode - True

    public static TreeNode creatSymTreeNode1() {

        TreeNode root = new TreeNode(1);
        TreeNode n2 = new TreeNode(2);
        TreeNode n3 = new TreeNode(2);
        TreeNode n4 = new TreeNode(3);
        TreeNode n5 = new TreeNode(4);
        TreeNode n6 = new TreeNode(4);
        TreeNode n7 = new TreeNode(3);

        root.left = n2;
        root.right = n3;
        n2.left = n4;
        n2.right = n5;
        n3.left = n6;
        n3.right = n7;
        return root;
    }

    //    create sample input TreeNode - False

    public static TreeNode creatNotSymTreeNode1() {

        TreeNode root = new TreeNode(1);
        TreeNode n2 = new TreeNode(2);
        TreeNode n3 = new TreeNode(2);
        TreeNode n4 = new TreeNode(3);
        TreeNode n5 = new TreeNode(4);
        TreeNode n6 = new TreeNode(4);

        root.left = n2;
        root.right = n3;
        n2.left = n4;
        n2.right = n5;
        n3.left = n6;

        return root;
    }

    public static void main(String args[]){

        TreeNode root = creatSymTreeNode1();

        TreeNode root1 = creatNotSymTreeNode1();

        System.out.println(isSymmetric(root)); // true

        System.out.println(isSymmetric(root1)); // false

    }

}


/**
 * Class of TreeNode for reference
 */


 class TreeNode {

    int val;

    TreeNode left;

    TreeNode right;

    TreeNode(int val) { this.val = val; }

    }