/**
 * Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.
 *
 * Example:
 *
 * Input: [-2,1,-3,4,-1,2,1,-5,4],
 * Output: 6
 * Explanation: [4,-1,2,1] has the largest sum = 6.
 */
public class MaximumSubarray {

    public static int maxSubArray(int[] nums){

        if(nums.length == 0) return 0;

        int curr = nums[0];

        int max = nums[0];

        for(int i=1; i<nums.length; i++){

            curr = Math.max(nums[i], curr+nums[i]); // curr+nums[i]

            max = Math.max(max, curr);

        }

        return max;

    }

    public static void main(String args[]){
        int[] nums = {-2,1,-3,4,-1,2,1,-5,4};

        System.out.println(Integer.toString(maxSubArray(nums))); // 6
    }
}
