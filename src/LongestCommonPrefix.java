/**
 * Write a function to find the longest common prefix string amongst an array of strings.
 * If there is no common prefix, return an empty string "".
 *
 * Example 1:
 *
 * Input: ["flower","flow","flight"]
 * Output: "fl"
 *
 * Example 2:
 *
 * Input: ["dog","racecar","car"]
 * Output: ""
 * Explanation: There is no common prefix among the input strings.
 */



public class LongestCommonPrefix {

    /**
     * nested for loops
     */

    public static String LongestCommonPrefix1(String[] strs){

        if(strs == null || strs.length == 0) return "";

        //set first str as prefix

        String prefix = strs[0];

        for(int i = 1; i < strs.length; i++){

            //compare first str with every string

            while(strs[i].indexOf(prefix) != 0){

                // if not match reset prefix str - took last letter from the str.

                prefix = prefix.substring(0, prefix.length()-1);

            }

        }

        return prefix;

    }

    /**
     * recursive method
     * input: "a","ab","abc","abcd","abcde","abcdef","abcdefg","abcdefgh"
     *
     * ["a","ab"], ["abc","abcd"], ["abcde","abcdef"], ["abcdefg","abcdefgh"]
     *     |             |                  |                     |
     *   ["a"],       ["abc"],          ["abcde"],           ["abcdefg"]
     *            |                                     |
     *          ["a"],                              ["abcde"]
     *                             |
     *                           ["a"]
     */

    public static String LongestCommonPrefix2(String[] strs){

        // when string arrays empty return ""

        if(strs == null || strs.length == 0) return "";

        //else visit LongestCommonPrefix2 with three inputs

        return LongestCommonPrefix2(strs, 0 ,strs.length - 1);

    }

    public static String LongestCommonPrefix2(String[] strs, int l, int r){

        //when array has only one str return that str

        if(l == r) {

            return strs[l];

        }

        else{

            // when array contains more than one str, find mid str index

            int mid = (l + r)/2;

            String lcpLeft = LongestCommonPrefix2(strs, l, mid);

            String lcpRight = LongestCommonPrefix2(strs, mid + 1, r);

            return commonPrefix(lcpLeft, lcpRight);
        }

    }

    public static String commonPrefix(String left, String right){

        //find smaller length

        int min = Math.min(left.length(), right.length());

        for(int i = 0; i < min; i++){

            if(left.charAt(i) != right.charAt(i)) return left.substring(0, i);

        }

        return left.substring(0, min);

    }

    public static void main(String args[]){

        String[] input1 = {"a","ab","abc","abcd","abcde","abcdef","abcdefg","abcdefgh"};

        String[] input2 = {"dog","racecar","car","c"};

        System.out.println(LongestCommonPrefix1(input1));

        System.out.println(LongestCommonPrefix1(input2));

        System.out.println(LongestCommonPrefix2(input1));

        System.out.println(LongestCommonPrefix2(input2));

    }
}
