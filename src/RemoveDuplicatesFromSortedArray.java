/**
 * Given a sorted array nums, remove the duplicates in-place such that each element appear only once and return the new length.
 *
 * Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.
 *
 * Given nums = [0,0,1,1,1,2,2,3,3,4],
 *
 * Your function should return length = 5, with the first five elements of nums being modified to 0, 1, 2, 3, and 4 respectively.
 *
 * It doesn't matter what values are set beyond the returned length.
 *
 *
 * // nums is passed in by reference. (i.e., without making a copy)
 * int len = removeDuplicates(nums);
 *
 * // any modification to nums in your function would be known by the caller.
 * // using the length returned by your function, it prints the first len elements.
 * for (int i = 0; i < len; i++) {
 *     print(nums[i]);
 * }
 */

public class RemoveDuplicatesFromSortedArray {

    public static int removeDuplicates(int[] nums){

            //when input num is single int, return 0 index

            if (nums.length==0) return 0;

            // set initial index, this index only increase when meet different number, if has same numbers in a row, not increase.

            int i = 0;

            // iterate input start from the next index

            for( int j =1; j < nums.length; j++){

                //if the next number different than previous one

                if(nums[j]!=nums[i]){

                    //only increase the previous index when two numbers are different

                    i++;

                    //set the increased index to different number.

                    nums[i]=nums[j];

                }

            }

            //return +1 since index started from 0

            return i+1;


    }

    public static void main(String args[]){

        int[] input1 = {1,1,2};

        int[] input2 = {0,0,1,1,1,2,2,3,3,4};

        System.out.println(Integer.toString(removeDuplicates(input1))); // return 2 , num = [1, 2, 2]

        System.out.println(Integer.toString(removeDuplicates(input2))); // return 5 , num = [0, 1, 2, 3, 4, 2, 2, 3, 3, 4]

    }
}
