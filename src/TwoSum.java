import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Given an array of integers, return indices of the two numbers such that they add up to a specific target.
 * You may assume that each input would have exactly one solution, and you may not use the same element twice.
 *
 * Given nums = [2, 7, 11, 15], target = 9,
 *
 * Because nums[0] + nums[1] = 2 + 7 = 9,
 * return [0, 1].
 */

//slow method
public class TwoSum {

    /**
     * Method one - nested loop - slow O(n2)
     */

    public static int[] towSum1(int[] nums, int target){

        //First for loop to iterate entire input array

        for(int i = 0; i < nums.length; i++){

            //In each iterate of outer loop, loop input array again start from the next index to find the target

            for(int j = i + 1; j < nums.length; j++){

                //When the value equals target - current index value, return indexes of two values.

                if(nums[j] == target - nums[i]){

                    return new int[] {i, j};

                }

            }

        }

        //No matches throw no solution message

        throw new IllegalArgumentException("no solution");

    }

    /**
     * Method two - two pass hash table - O(n)
     */

    public static int[] towSum2(int[] nums, int target){

        //Create empty hash map

        Map<Integer, Integer> map = new HashMap<>();

        //put all input arrays in created hash map

        for(int i = 0; i < nums.length; i++){

            map.put(nums[i], i);

        }

        //iterate input arrays to find the matching value of target - nums[i] from hashmap

        for(int i = 0; i < nums.length; i++) {

            int complement = target - nums[i];

            //when found the value from hash map return current index from input array and the index from hash map

            if(map.containsKey(complement) && map.get(complement) != i){

                return new int[] { i, map.get(complement)};

            }

        }

        //No matches throw no solution message

        throw new IllegalArgumentException("no solution");
    }

    /**
     * Method three - one pass hash table - O(n)
     */

    public static int[] towSum3(int[] nums, int target){

        //when null of empty input array return 0

        if(nums == null || nums.length == 0) return new int[0];

        //create empty hash map

        Map<Integer, Integer> map = new HashMap<>();

        //loop input array and each iterate add the value into hash map

        for(int i = 0; i < nums.length; i++){

            //when value of target - nums[i] exist in hash map return current index from input array and the index from hash map

            if(map.containsKey(target - nums[i])){

                return new int[] { i, map.get(target - nums[i])};

            }

            //when value of target - nums[i] not in hash map, add current value to hash map

            map.put(nums[i], i); // to avoid iterate twice in hashtable

        }

        throw new IllegalArgumentException("no solution");

    }

    public static void main(String args[]){

        int[] nums = {2, 7, 11, 15};

        int target = 9;

        //To print int[] need Arrays.toString()

        System.out.println(Arrays.toString(towSum1(nums,target))); // return [0,1]

        System.out.println(Arrays.toString(towSum2(nums,target))); // return [0,1]

        System.out.println(Arrays.toString(towSum3(nums,target))); // return [0,1]

    }

}
