import java.util.Stack;

/**
 * Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.
 *
 * An input string is valid if:
 *
 * Open brackets must be closed by the same type of brackets.
 * Open brackets must be closed in the correct order.
 * Note that an empty string is also considered valid.
 *
 * Example 1:
 * Input: "()"
 * Output: true
 *
 * Example 2:
 * Input: "()[]{}"
 * Output: true
 *
 * Example 3:
 * Input: "(]"
 * Output: false
 *
 * Example 4:
 * Input: "([)]"
 * Output: false
 *
 * Example 5:
 * Input: "{[]}"
 * Output: true
 */

public class ValidParentheses {

    /**
     * using stack to collect closing parenthesis - if open parenthesis push close parenthesis
     * then when iterate to closing parenthesis using pop method to check that its matches, not match return false.
     */
    public static boolean isValid(String s) {

        Stack<Character> stack = new Stack<Character>();

        char[] input = s.toCharArray();

        for(char c : input){

            if(c == '(') stack.push(')');

            else if(c == '{') stack.push('}');

            else if(c == '[') stack.push(']');

            else if(stack.isEmpty() || stack.pop() != c) return false;

        }

        return stack.isEmpty();
    }

    public static void main(String args[]) {

        String input1 = "()";

        String input2 = "()[]{}";

        String input3 = "(]";

        String input4 = "([)]";

        String input5 = "{[]}";

        System.out.println(isValid(input1));

        System.out.println(isValid(input2));

        System.out.println(isValid(input3));

        System.out.println(isValid(input4));

        System.out.println(isValid(input5));

    }

}
