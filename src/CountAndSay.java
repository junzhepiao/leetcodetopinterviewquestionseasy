/**
 *The count-and-say sequence is the sequence of integers with the first five terms as following:
 *
 * 1.     "1"
 * 2.     "11"
 * 3.     "21"
 * 4.     "1211"
 * 5.     "111221"
 * 1 is read off as "one 1" or 11.
 * 11 is read off as "two 1s" or 21.
 * 21 is read off as "one 2, then one 1" or 1211.
 *
 * Given an integer n where 1 ≤ n ≤ 30, generate the nth term of the count-and-say sequence. You can do so recursively, in other words from the previous member read off the digits, counting the number of digits in groups of the same digit.
 *
 * Note: Each term of the sequence of integers will be represented as a string.
 *
 *
 *
 * Example 1:
 *
 * Input: 1
 * Output: "1"
 * Explanation: This is the base case.
 * Example 2:
 *
 * Input: 4
 * Output: "1211"
 * Explanation: For n = 3 the term was "21" in which we have two groups "2" and "1", "2" can be read as "12" which means frequency = 1 and value = 2, the same way "1" is read as "11", so the answer is the concatenation of "12" and "11" which is "1211".
 */

public class CountAndSay {

    public static String countAndSay(int n){

        if(n==1) return "1";

        return analyze(countAndSay(n-1)); //recursive call, calculate from bottom n = 1, then increase to n.

    }

    public static String analyze(String res){

        StringBuilder b = new StringBuilder(); // mutable, avoid create new objects in evert iteration

        int count =1;

        char curr = res.charAt(0);

        for(int i=1; i<res.length(); i++){

            char atI = res.charAt(i);

            if(atI != curr){

                b.append(count).append(curr);

                curr = atI;

                count = 1;

            }else{

                count++;

            }

        }

        b.append(count).append(curr);

        return b.toString();

    }

    public static void main(String args[]){

        System.out.println(countAndSay(5));

        /**
         *       STEP:
         *       n=5, countAndSay(5) -> perform recursive
         *       n=4, analyze( countAndSay( 4 ))
         *       n=3, analyze( countAndSay( analyze( countAndSay( 3 ))  ))
         *       n=2, analyze( countAndSay( analyze( countAndSay( analyze( countAndSay( 2 ))  ))  ))
         *       n=1, analyze( countAndSay( analyze( countAndSay( analyze( countAndSay( analyze( countAndSay( 1 ))  ))  ))  )) -> recursive done.
         *
         *       n=1, analyze(  analyze(  analyze(  analyze( "1" )  )  )  ) -> start resolve from inside
         *       n=2, analyze(  analyze(  analyze(  "11"  )  )  )
         *       n=3, analyze(  analyze(  "21"  )  )
         *       n=4, analyze(  "1211"  )
         *       n=5, "111221" -> done
         */

    }
}
