import com.sun.source.tree.Tree;

import java.util.Arrays;

/**
 * Given an array where elements are sorted in ascending order, convert it to a height balanced BST.
 *
 * For this problem, a height-balanced binary tree is defined as a binary tree in which the depth of the two subtrees of every node never differ by more than 1.
 *
 * Example:
 *
 * Given the sorted array: [-10,-3,0,5,9],
 *
 * One possible answer is: [0,-3,9,-10,null,5], which represents the following height balanced BST:
 *
 *       0
 *      / \
 *    -3   9
 *    /   /
 *  -10  5
 */
public class ConvertSortedArraytoBinarySearchTree {

    public static TreeNode sortedArrayToBST(int[] nums){

        int n = nums.length - 1;

        return computeBST(nums,0, n);

    }

    public static TreeNode computeBST(int[] nums, int start, int end){

        if(start>end) return null;

        int mid = (start+end)/2;

        TreeNode root = new TreeNode(nums[mid]);

        root.left = computeBST(nums,start, mid-1);

        root.right = computeBST(nums,mid+1, end);

        return root;

    }

    // create sample node to test

    public static TreeNode sample() {

        TreeNode root = new TreeNode(0);
        TreeNode n2 = new TreeNode(-10);
        TreeNode n3 = new TreeNode(5);
        TreeNode n5 = new TreeNode(-3);
        TreeNode n7 = new TreeNode(9);

        root.left = n2;
        root.right = n3;
        n2.right = n5;
        n3.right = n7;

        return root;
    }

    // test method to compare two TreeNode

    public static boolean identicalTrees(TreeNode a, TreeNode b)
    {
        /*1. both empty */
        if (a == null && b == null) return true;

        /* 2. both non-empty -> compare them */
        if (a != null && b != null)

            return (a.val == b.val

                    && identicalTrees(a.left, b.left)

                    && identicalTrees(a.right, b.right));

        /* 3. one empty, one not -> false */
        return false;

    }

    public static void main(String args[]){

        int[] nums = {-10,-3,0,5,9};

        TreeNode rootNode = sortedArrayToBST(nums);

        TreeNode sample = sample();

        System.out.println(identicalTrees(rootNode,sample)); // true

    }
}
