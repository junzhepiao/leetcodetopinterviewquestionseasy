
/**
 * Example:
 *
 * Input: 5
 * Output:
 * [
 *      [1],
 *     [1,1],
 *    [1,2,1],
 *   [1,3,3,1],
 *  [1,4,6,4,1]
 * ]
 */

import java.util.Arrays;
import java.util.List;

import java.util.ArrayList;

public class PascalsTriangle {

    public static List<List<Integer>> generate(int numRows) {

        // create empty list

        List<List<Integer>> res = new ArrayList<List<Integer>>();

        //add each list in  empty list

        for(int i = 0; i < numRows; i++){

            // each sub list start with 1

            List<Integer> list = new ArrayList<Integer>(Arrays.asList(1));

            //leave first and last num since both going to be 1

            for(int j = 1; j < i; j++){

                list.add(res.get(i-1).get(j-1)+res.get(i-1).get(j));

            }

            //each sub list end with 1

            if(i>0) list.add(1);

            //add generated sublist to main list

            res.add(list);

        }

        return res;

    }

    public static void main(String args[]){

        List res = generate(5);

        System.out.println(Arrays.toString(res.toArray()));
    }
}


//ps: equation for pascal's Triangle (Row)! / [(R-n)! * n!] : Row -> num of Row, n -> Nth number in each row