/**
 *  Given a 32-bit signed integer, reverse digits of an integer.
 *
 * Example 1:
 * Input: 123
 * Output: 321
 *
 * Example 2:
 * Input: -123
 * Output: -321
 *
 * Example 3:
 * Input: 120
 * Output: 21
 *
 * Assume we are dealing with an environment which could only store integers within the 32-bit signed integer range: [−231,  231 − 1].
 * For the purpose of this problem, assume that your function returns 0 when the reversed integer overflows.
 */

public class ReverseInteger {

    public static int ReverseInteger(int x){

        int reverse = 0;

        while(x != 0) {

            int remainder = x % 10;

            x /= 10;

            //if revered value is greater than Integer.MAX_VALUE / 10 or the final remainder is greater than 7 since MAX is 2147483647, then the remaining remainder is not considerable any more

            if( reverse > Integer.MAX_VALUE / 10 || (reverse == Integer.MAX_VALUE / 10 && remainder > 7)) return 0;

            //if revered value is less than Integer.MIN_VALUE / 10 or the final remainder is less than -8 since MIN is -2147483648, then the remaining remainder is not considerable any more

            if( reverse < Integer.MIN_VALUE / 10 || (reverse == Integer.MIN_VALUE / 10 && remainder < -8)) return 0;

            reverse = reverse * 10 + remainder;

        }

        return reverse;

    }

    public static void main(String args[]){

        int input1 = 123;

        int input2 = -123;

        int input3 = 120;

        int input4 = 2147483647;

        int input5 = -2147483648;

        System.out.println(Integer.toString(ReverseInteger(input1))); //return 321

        System.out.println(Integer.toString(ReverseInteger(input2))); //return -321

        System.out.println(Integer.toString(ReverseInteger(input3))); //return 21

        System.out.println(Integer.toString(ReverseInteger(input4))); //return 0

        System.out.println(Integer.toString(ReverseInteger(input5))); //return 0
    }

}
