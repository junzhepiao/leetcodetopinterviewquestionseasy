import java.util.Arrays;

/**
 * Given a non-empty array of digits representing a non-negative integer, increment one to the integer.
 *
 * The digits are stored such that the most significant digit is at the head of the list, and each element in the array contains a single digit.
 *
 * You may assume the integer does not contain any leading zero, except the number 0 itself.
 *
 * Example 1:
 *
 * Input: [1,2,3]
 * Output: [1,2,4]
 * Explanation: The array represents the integer 123.
 * Example 2:
 *
 * Input: [4,3,2,1]
 * Output: [4,3,2,2]
 * Explanation: The array represents the integer 4321.
 */

public class PlusOne {

    public static int[] plusOne(int[] digits){

        int add = 1;

        for(int i = digits.length-1; i>=0; i--){

            digits[i]+=add;

            if( digits[i]<=9) return digits; //if not exceed 9 just return array, method stops here.

            digits[i]=0; //means exceed

        }

        //to here means all value exceed 9, need add one more index

        int[] exceed = new int[digits.length+1];// no need to modify rest of the num since will be zero anyway

        exceed[0]=1;

        return exceed;

    }

    public static void main(String args[]){

        int[] input1 = {1,2,9};

        int[] input2 = {9,9};

        System.out.println(Arrays.toString(plusOne(input1)));

        System.out.println(Arrays.toString(plusOne(input2)));

    }

}
