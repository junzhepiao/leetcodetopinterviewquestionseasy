/**
 * Say you have an array for which the ith element is the price of a given stock on day i.
 *
 * If you were only permitted to complete at most one transaction (i.e., buy one and sell one share of the stock), design an algorithm to find the maximum profit.
 *
 * Note that you cannot sell a stock before you buy one.
 *
 * Example 1:
 *
 * Input: [7,1,5,3,6,4]
 * Output: 5
 * Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6), profit = 6-1 = 5.
 *              Not 7-1 = 6, as selling price needs to be larger than buying price.
 * Example 2:
 *
 * Input: [7,6,4,3,1]
 * Output: 0
 * Explanation: In this case, no transaction is done, i.e. max profit = 0.
 */

public class BestTimeToBuyAndSellStock {

    public static int maxProfit(int[] prices){

        int curMax = 0;

        int outputMax = 0;

        for(int i = 1; i < prices.length; i++){

            //add each gap to find max, if goes negative current-max start from 0.

            curMax += prices[i] - prices[i-1];

            if(curMax > outputMax) outputMax = curMax;

            if(curMax < 0) curMax = 0;

        }

        return outputMax;

    }

    public static void main(String args[]){

        int[] prices = {2,4,6,0,3};

        System.out.println(Integer.toString(maxProfit(prices))); // 4

    }

}
